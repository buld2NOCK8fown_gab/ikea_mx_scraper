import csv
import json
import logging
import math
import re
import time
import warnings
from datetime import datetime

import jinja2
import pandas as pd
from bs4 import BeautifulSoup
from selenium import webdriver
from selenium.webdriver.support.wait import WebDriverWait
from snowflake.sqlalchemy import URL
from sqlalchemy import create_engine

warnings.filterwarnings("ignore")
logging.basicConfig(
    level=logging.INFO,
    filename="log_selenium_test.log",
    filemode="a",
    format="%(asctime)s :: %(levelname)s :: %(message)s",
)
log = logging.getLogger("__name__")

chrome_opt = webdriver.ChromeOptions()
chrome_opt.add_experimental_option("excludeSwitches", ["enable-automation"])
chrome_opt.add_experimental_option("useAutomationExtension", False)
chrome_opt.add_argument(
    "user-agent=Mozilla/5.0 (Macintosh; Intel Mac OS X 10.13; rv:63.0) Gecko/20100101 Firefox/63.0"
)
driver = webdriver.Chrome(chrome_options=chrome_opt)
driver.maximize_window()
wait = WebDriverWait(driver, 10)

def get_dom(url):
    """
    gets the DOM of the requested url using selenium
    """
    driver.get(url)


def do_smooth_scroll():
    """
    Smoothly scrolls the current page all the way to the bottom,
    giving time to results to load.
    """
    time.sleep(3)
    total_height = int(driver.execute_script(
        "return document.body.scrollHeight"))

    for i in range(1, total_height, 5):
        driver.execute_script("window.scrollTo(0, {});".format(i))

    driver.execute_script(
        "window.scrollTo(0, document.body.scrollHeight - 2100);")


def main():
    url = "https://www.ikea.com/mx/es/cat/accesorios-para-velas-16307/"
    get_dom(url)
    time.sleep(2)
    do_smooth_scroll()
    cat_soup = BeautifulSoup(driver.page_source, "html.parser")
    total_products_tag = cat_soup.find(
        "div", {"class": "catalog-product-list__total-count"}
    )
    if total_products_tag:
        total_products = re.findall(
            r"\d+", total_products_tag.text.strip())[-1]
        pages = int(math.ceil(float(total_products) / 24))
        full_prods_url = url + "?page=" + str(pages)
        get_dom(full_prods_url)
        do_smooth_scroll()
        driver.execute_script("window.scrollTo(0, 1080);")
        cat_soup = BeautifulSoup(driver.page_source, "html.parser")

        results = cat_soup.find_all("div", {"data-testid": "plp-product-card"})

        log.info("total_products_tag: %s", total_products_tag)
        log.info("total products: %s", total_products)
        log.info("pages: %i", pages)
        log.info("full_prods_url: %s", full_prods_url)
        log.info("results: %s", str(len(results)))

    else:
        results = []
        log.info(">>> Página sin productos listados.")
        log.info(url)
        log.info(">>>")


if __name__ == "__main__":
    main()
