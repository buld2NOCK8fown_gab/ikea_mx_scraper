"""
Author: Alain Moré Maceda
Scraper de productos IKEA MX
"""
import csv
import json
import logging
import math
import re
import time
import warnings
from datetime import datetime

import jinja2
import pandas as pd
from bs4 import BeautifulSoup
from selenium import webdriver
from selenium.webdriver.support.wait import WebDriverWait
from snowflake.sqlalchemy import URL
from sqlalchemy import create_engine

warnings.filterwarnings("ignore")
logging.basicConfig(
    level=logging.INFO,
    filename="log_scraper.log",
    filemode="a",
    format="%(asctime)s :: %(levelname)s :: %(message)s",
)
log = logging.getLogger("__name__")

chrome_opt = webdriver.ChromeOptions()
chrome_opt.add_experimental_option("excludeSwitches", ["enable-automation"])
chrome_opt.add_experimental_option("useAutomationExtension", False)
chrome_opt.add_argument(
    "user-agent=Mozilla/5.0 (Macintosh; Intel Mac OS X 10.13; rv:63.0) Gecko/20100101 Firefox/63.0"
)
driver = webdriver.Chrome(chrome_options=chrome_opt)
driver.maximize_window()
wait = WebDriverWait(driver, 10)


def get_dom(url):
    """
    gets the DOM of the requested url using selenium. NA
    """
    driver.get(url)


def do_smooth_scroll():
    """
    Smoothly scrolls the current page all the way to the bottom,
    giving time to results to load.
    """
    time.sleep(3)
    total_height = int(driver.execute_script("return document.body.scrollHeight"))

    for i in range(1, total_height, 5):
        driver.execute_script("window.scrollTo(0, {});".format(i))

    driver.execute_script("window.scrollTo(0, document.body.scrollHeight - 2100);")


def get_product_dimensions():
    """
    Navigate the product details page to get the product dimensions
    """
    try:
        # Boton "Medidas"
        # driver.find_element_by_xpath(
        #     "//button[@class='range-revamp-chunky-header']"
        # ).click()
        driver.find_element_by_xpath(
            "//span[text()='Medidas']"
        ).click()
        log.info("Boton1 Medidas presionado")
        time.sleep(0.5)
        #Boton "Embalaje"
        # driver.find_element_by_xpath(
        #     "//button[@aria-controls='SEC_product-details-packaging']"
        # ).click()

        # PRODUCT DIMENSIONS
        try:
            prod_soup = BeautifulSoup(driver.page_source, "html.parser")

            # dim_tag = prod_soup.find("div", {"id": "SEC_product-details-packaging"})
            dim_tag = prod_soup.find("div", {"id": "SEC_dimensions-packaging"})

            dimensions = []
            # dimension_tags = dim_tag.find_all(
            #     "span", {"class": "range-revamp-product-details__label"}
            # )
            dimension_tags = dim_tag.find_all(
                "p"
            )
            for dim_tag in dimension_tags:
                dim_dict = {}
                dim_key = ((dim_tag.text.strip().split(":"))[0].strip()).replace(
                    "\\u00e1", "á"
                )
                dim_value = (dim_tag.text.strip().split(":"))[1].strip()
                dim_dict[dim_key] = dim_value
                dimensions.append(dim_dict)

            dim_json = json.dumps(dimensions)
            log.info("Medidas obtenidas OK")
            return dim_json

        except Exception as ex:
            log.info("No se pudieron obtener dimensiones")
            log.error(ex)
            return None
    except Exception as ex_btn:
        log.info("No se pudo presionar botones")
        log.error(ex_btn)
        return None


def insert_product(connection, product):
    """
    Inserts current processed product into Snowflake
    """
    insert = """
        INSERT INTO MX_WRITABLE.IKEA_PRODUCTS 
            (CATEGORY,URL,NAME,DESC_1,DESC_2,PRICE_1,PRICE_2,ONLINE_AVAILABILITY,
                STORE_AVAILABILITY,PICTURES,SUMMARY,PRODUCT_NUMBER,DIMENSIONS,
                SCRAPE_DATETIME)
        VALUES(
            '{{prod_category}}',
            '{{prod_url}}',
            '{{prod_name}}',
            '{{desc_1}}',
            '{{desc_2}}',
            {{price_1}},
            '{{price_2}}',
            '{{status_1}}',
            '{{status_2}}',
            '{{pictures}}',
            '{{summary}}',
            '{{prod_number}}',
            '{{prod_dimensions}}',
            '{{scrape_datetime}}'  
        )     
    """
    template = jinja2.Environment(loader=jinja2.BaseLoader).from_string(insert)
    formatted_template = template.render(**product)
    log.info(formatted_template)
    try:
        df_insert = pd.read_sql_query(con=connection, sql=formatted_template)
        log.info("Insert OK")
        log.info(df_insert)
    except Exception as ex:
        log.info("Insert ERROR")
        log.error(ex)


def start_scraping(url, connection):
    """
    Scraping main function, orchestrates full process
    """

    # En este bloque se obtienen los productos de la categoria.
    # Para esto se hace obtienen primero el total de productos en
    # la parte de abajo de la primera página. Como hay 24 productos max por
    # pagina, dividimos el total por 24, obtenemos el total de paginas,
    # y abrimos la pagina con todos los productos.
    # Finalmente, hacemos otro smooth scroll de toda la pagina, para que carguen
    # todos los productos.
    get_dom(url)
    do_smooth_scroll()
    cat_soup = BeautifulSoup(driver.page_source, "html.parser")
    prod_category = cat_soup.find("h1", {"class": "plp-page-title__title"}).text.strip()
    total_products_tag = cat_soup.find(
        "div", {"class": "catalog-product-list__total-count"}
    )
    total_products = re.findall(r"\d+", total_products_tag.text.strip())[-1]
    pages = int(math.ceil(float(total_products) / 24))
    full_prods_url = url + "?page=" + str(pages)
    get_dom(full_prods_url)
    do_smooth_scroll()
    cat_soup = BeautifulSoup(driver.page_source, "html.parser")

    results = cat_soup.find_all("div", {"data-testid": "plp-product-card"})

    log.info(total_products_tag)
    log.info(str(len(results)))

    # Obtener los detalles de cada producto, desde la pagina de resultados
    products = []
    for result in results:
        product_info = []

        prod_name = (
            result.find(
                "div",
                {"class": "range-revamp-header-section__title--small notranslate"},
            )
        ).text.strip()
        log.info(prod_name)

        desc_1 = ""
        desc_2 = ""
        price_1 = ""
        price_2 = ""
        # status_1 = ""
        status_2 = "No Info"
        prod_url = ""

        try:
            desc_1 = (
                result.find(
                    "span", {"class": "range-revamp-header-section__description-text"}
                )
            ).text.strip()
        except Exception as ex:
            log.info("No se pudo obtener desc_1")
            log.error(ex)

        try:
            desc_2 = (
                result.find(
                    "span",
                    {"class": "range-revamp-header-section__description-measurement"},
                )
            ).text.strip()
        except Exception as ex:
            log.info("No se pudo obtener desc_2")
            log.error(ex)

        try:
            price_1 = (
                result.find("span", {"class": "range-revamp-price__integer"})
            ).text.strip()
        except Exception as ex:
            log.info("No se pudo obtener price_1")
            log.error(ex)

        try:
            price_2 = (
                result.find("span", {"class": "range-revamp-price__unit"})
            ).text.strip()
        except Exception as ex:
            log.info("No se pudo obtener price_2")
            log.error(ex)

        # try:
        #     status_1_and_2 = result.find_all("p", {"class": "plp-status__label"})
        #     status_1 = (status_1_and_2[0]).text.strip()
        #     status_2 = (status_1_and_2[1]).text.strip()
        # except Exception as ex:
        #     log.info("No se pudo obtener status_1 o status_2")
        #     log.error(ex)

        try:
            prod_url = result.find(
                "a", {"class": "range-revamp-product-compact__wrapper-link"}
            )["href"]
        except Exception as ex:
            log.info("No se pudo obtener prod_url")
            log.error(ex)

        product_info = {
            "prod_category": prod_category,
            "prod_url": prod_url,
            "prod_name": prod_name,
            "desc_1": desc_1,
            "desc_2": desc_2,
            "price_1": float(price_1.replace(" ", "")),
            "price_2": price_2,
            # "status_1": status_1,
            "status_2": status_2,
        }
        products.append(product_info)

    # Obtener detalles desde la página del producto
    log.info("")
    for product in products:
        get_dom(product["prod_url"])
        time.sleep(2)
        prod_soup = BeautifulSoup(driver.page_source, "html.parser")

        # PICTURES
        try:
            pictures = []
            picture_tags = prod_soup.find_all(
                "img", {"class": "range-revamp-aspect-ratio-image__image"}
            )
            for pic_tag in picture_tags:
                pictures.append(pic_tag["src"])

            pics_json = json.dumps(pictures)
            product["pictures"] = pics_json
        except Exception as ex:
            log.info("No se pudieron obtener imagenes")
            log.error(ex)

        # SUMMARY
        try:
            summary = (
                prod_soup.find(
                    "p", {"class": "range-revamp-product-summary__description"}
                )
                .text.strip()
                .replace("'", "")
            )
            product["summary"] = summary
        except Exception as ex:
            log.info("No se pudo obtener summary")
            log.error(ex)

        # PRODUCT NUMBER
        try:
            prod_number = prod_soup.find(
                "span", {"class": "range-revamp-product-identifier__value"}
            ).text.strip()
            product["prod_number"] = prod_number
        except Exception as ex:
            log.info("No se pudo obtener prod_number")
            log.error(ex)

        # Online availability
        try:
            status_1 = (prod_soup.find(
                "span", {"class":"range-revamp-delivery__text"}
            ).text.strip().split("."))[0]
            product["status_1"] = status_1
        except Exception as ex:
            log.info("No se pudo obtener online stock")
            log.error(ex)

        # PRODUCT DIMENSIONS
        prod_dimensions = get_product_dimensions()
        if prod_dimensions:
            product["prod_dimensions"] = prod_dimensions
        else:
            log.info("No se pudo obtener prod_dimensions")

        product["scrape_datetime"] = datetime.now()

        insert_product(connection, product)
        log.info(product)
        log.info("")
        log.info("")
        log.info(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>")
        log.info("")
        log.info("")

        # log.info("Inserting to csv")
        # with open("ikea.csv", "a", encoding="utf-8", newline="") as csvoutput:
        #     csvwriter = csv.writer(csvoutput, delimiter=",")
        #     csvwriter.writerow(product)
        # log.info("")


def main():
    """
    Función main, ejecuta el proceso paso por paso
    """
    print("PROCESS START!")
    snowflake_configuration = {
        "url": "hg51401.snowflakecomputing.com",
        "account": "hg51401",
        "user": "ALAIN.MORE@RAPPI.COM",
        "authenticator": "externalbrowser",
        "port": 443,
        "warehouse": "GROWTH_ANALYSTS",
        "role": "GROWTH_ROLE",
        "database": "fivetran",
    }
    engine = create_engine(URL(**snowflake_configuration))
    connection = engine.connect()
    try:
        with open("categories.csv", "r") as csvfile:
            csvreader = csv.DictReader(csvfile)
            for row in csvreader:
                cat_url = row["category_url"]
                start_scraping(cat_url, connection)

    finally:
        connection.close()
        engine.dispose()
        log.info("Engine y connection cerrados")


if __name__ == "__main__":
    main()
