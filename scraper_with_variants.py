"""
Author: Alain Moré Maceda
Scraper de productos IKEA MX
"""
import csv
import itertools
import json
import logging
import math
import re
import time
import warnings
from datetime import datetime

import jinja2
import pandas as pd
from bs4 import BeautifulSoup
from selenium.webdriver.common.action_chains import ActionChains
from selenium import webdriver
from selenium.webdriver.support.wait import WebDriverWait
from snowflake.sqlalchemy import URL
from sqlalchemy import create_engine

warnings.filterwarnings("ignore")
logging.basicConfig(
    level=logging.INFO,
    filename="log_scraper.log",
    filemode="a",
    format="%(asctime)s :: %(levelname)s :: %(message)s",
)
log = logging.getLogger("__name__")

chrome_opt = webdriver.ChromeOptions()
chrome_opt.add_experimental_option("excludeSwitches", ["enable-automation"])
chrome_opt.add_experimental_option("useAutomationExtension", False)
chrome_opt.add_argument(
    "user-agent=Mozilla/5.0 (Macintosh; Intel Mac OS X 10.13; rv:63.0) Gecko/20100101 Firefox/63.0"
)
driver = webdriver.Chrome(chrome_options=chrome_opt)
driver.maximize_window()
wait = WebDriverWait(driver, 10)
actions = ActionChains(driver)


def set_query_tag(connection):
    select = """
        ALTER SESSION SET QUERY_TAG = "global-ecommerce-data";
    """
    template = jinja2.Environment(loader=jinja2.BaseLoader).from_string(select)
    formatted_template = template.render()
    log.info(formatted_template)
    try:
        df_query_tag = pd.read_sql_query(
            con=connection, sql=formatted_template)
        log.info(df_query_tag)
    except Exception as ex:
        log.error(ex)


def get_dom(url):
    """
    gets the DOM of the requested url using selenium NA
    """
    driver.get(url)


def do_smooth_scroll():
    """
    Smoothly scrolls the current page all the way to the bottom,
    giving time to results to load.
    """
    time.sleep(3)
    total_height = int(driver.execute_script(
        "return document.body.scrollHeight"))

    for i in range(1, total_height, 5):
        driver.execute_script("window.scrollTo(0, {});".format(i))

    driver.execute_script(
        "window.scrollTo(0, document.body.scrollHeight - 2100);")


def get_product_dimensions():
    """
    Navigate the product details page to get the product dimensions
    """
    try:
        # driver.find_element_by_xpath(
        #     "//button[@class='range-revamp-chunky-header']"
        # ).click()
        driver.find_element_by_xpath(
            "//span[text()='Medidas']"
        ).click()
        log.info("Boton1 Medidas presionado")
        time.sleep(0.5)
        # driver.find_element_by_xpath(
        #     "//button[@aria-controls='SEC_product-details-packaging']"
        # ).click()
        # log.info("Boton2 presionado")

        # PRODUCT DIMENSIONS
        try:
            prod_soup = BeautifulSoup(driver.page_source, "html.parser")

            # dim_tag = prod_soup.find("div", {"id": "SEC_product-details-packaging"})
            # dim_tag = prod_soup.find("div", {"id": "SEC_dimensions-packaging"})
            dim_tag = prod_soup.find("div", {"class": "pip-product-dimensions"})

            dimensions = []
            # dimension_tags = dim_tag.find_all(
            #     "span", {"class": "range-revamp-product-details__label"}
            # )
            dimension_tags = dim_tag.find_all(
                "p"
            )
            for dim_tag in dimension_tags:
                dim_dict = {}
                dim_key = ((dim_tag.text.strip().split(":"))[0].strip()).replace(
                    "\\u00e1", "á"
                )
                dim_value = (dim_tag.text.strip().split(":"))[1].strip()
                dim_dict[dim_key] = dim_value
                dimensions.append(dim_dict)

            dim_json = json.dumps(dimensions)
            log.info("Medidas obtenidas OK")
            return dim_json

        except Exception as ex:
            log.info("No se pudieron obtener dimensiones")
            log.error(ex)
            return None
    except Exception as ex_btn:
        log.info("No se pudo presionar botones")
        log.error(ex_btn)
        return None


def xpath_soup(element):
    """
    Generate xpath of soup element
    :param element: bs4 text or node
    :return: xpath as string
    """
    components = []
    child = element if element.name else element.parent
    for parent in child.parents:
        """
        @type parent: bs4.element.Tag
        """
        previous = itertools.islice(
            parent.children, 0, parent.contents.index(child))
        xpath_tag = child.name
        xpath_index = sum(1 for i in previous if i.name == xpath_tag) + 1
        components.append(
            xpath_tag if xpath_index == 1 else "%s[%d]" % (
                xpath_tag, xpath_index)
        )
        child = parent
    components.reverse()
    return "/%s" % "/".join(components)


def cleanup_snowflake_tables(connection):
    continue_process = True
    log.info("Cloning backup data")
    clone = "CREATE OR REPLACE TABLE MX_WRITABLE.IKEA_PRODUCTS_BCK CLONE MX_WRITABLE.IKEA_PRODUCTS"
    template = jinja2.Environment(loader=jinja2.BaseLoader).from_string(clone)
    formatted_template = template.render()
    log.info(formatted_template)
    try:
        df_clone = pd.read_sql_query(con=connection, sql=formatted_template)
        log.info("Clone OK")
        log.info(df_clone)
    except Exception:
        continue_process = False
        log.exception("Clone ERROR")

    if continue_process:
        log.info("Deleting old data")
        delete = "DELETE FROM MX_WRITABLE.IKEA_PRODUCTS"
        template = jinja2.Environment(
            loader=jinja2.BaseLoader).from_string(delete)
        formatted_template = template.render()
        log.info(formatted_template)
        try:
            df_delete = pd.read_sql_query(
                con=connection, sql=formatted_template)
            log.info("Delete OK")
            log.info(df_delete)
        except Exception:
            continue_process = False
            log.Exception("Delete ERROR")

    return continue_process


def insert_product(connection, product):
    """
    Inserts current processed product into Snowflake
    """
    insert = """
        INSERT INTO MX_WRITABLE.IKEA_PRODUCTS
            (CATEGORY,URL,NAME,DESC_1,DESC_2,PRICE_1,PRICE_2,ONLINE_AVAILABILITY,
                STORE_AVAILABILITY,PICTURES,SUMMARY,PRODUCT_NUMBER,DIMENSIONS,
                SCRAPE_DATETIME)
        VALUES(
            '{{prod_category}}',
            '{{prod_url}}',
            '{{prod_name}}',
            '{{desc_1}}',
            '{{desc_2}}',
            {{price_1}},
            '{{price_2}}',
            '{{status_1}}',
            '{{status_2}}',
            '{{pictures}}',
            '{{summary}}',
            '{{prod_number}}',
            '{{prod_dimensions}}',
            '{{scrape_datetime}}'
        )
    """
    template = jinja2.Environment(loader=jinja2.BaseLoader).from_string(insert)
    formatted_template = template.render(**product)
    log.info(formatted_template)
    try:
        df_insert = pd.read_sql_query(con=connection, sql=formatted_template)
        log.info("Insert OK")
        log.info(df_insert)
    except Exception as ex:
        log.info("Insert ERROR")
        log.error(ex)


def get_prod_details_from_listing(prod_category, result):
    """
    Obtiene los datos de los productos desplegados desde el
    listing de categoría. Considera todas las posibles variantes
    de un producto como un producto diferente.
    """
    # prod_name = ""
    desc_1 = ""
    desc_2 = ""
    price_1 = ""
    price_2 = ""
    # status_1 = ""
    # status_2 = "No Info"
    prod_url = ""

    # prod_name = (
    #     result.find(
    #         "span",
    #         {"class": "pip-header-section__title--small notranslate"},
    #         #{"class": "range-revamp-header-section__title--small notranslate"},
    #     )
    # ).text.strip()
    # log.info(prod_name)

    desc_1_tag = result.find(
        "span", {"class": "pip-header-section__description-text"}
        # "span", {"class": "range-revamp-header-section__description-text"}
    )
    if desc_1_tag:
        desc_1 = desc_1_tag.text.strip()
    else:
        log.info("No se pudo obtener desc_1")

    desc_2_tag = result.find(
        "span", {"class": "pip-header-section__description-measurement"}
        # "span", {"class": "range-revamp-header-section__description-measurement"}
    )
    if desc_2_tag:
        desc_2 = desc_2_tag.text.strip()
    else:
        log.info("No se pudo obtener desc_2")

    price_1_tag = result.find("span", {"class": "pip-price__integer"})
    # price_1_tag = result.find("span", {"class": "range-revamp-price__integer"})
    if price_1_tag:
        price_1 = price_1_tag.text.strip()
    else:
        log.info("No se pudo obtener price_1")

    price_2_tag = result.find("span", {"class": "pip-price__unit"})
    # price_2_tag = result.find("span", {"class": "range-revamp-price__unit"})
    if price_2_tag:
        price_2 = price_2_tag.text.strip()
    else:
        log.info("No se pudo obtener price_2")

    # status_1_and_2 = result.find_all("p", {"class": "plp-status__label"})
    # if status_1_and_2:
    #     if len(status_1_and_2) > 0:
    #         status_1 = (status_1_and_2[0]).text.strip()
    #     else:
    #         log.info("No se pudo obtener status_1")
    #     if len(status_1_and_2) > 1:
    #         status_2 = (status_1_and_2[1]).text.strip()
    #     else:
    #         log.info("No se pudo obtener status_2")
    # else:
    #     log.info("No se pudo obtener ni status_1 ni status_2")

    prod_url_tag = result.find(
        "a", {"class": "pip-product-compact__wrapper-link"}
        # "a", {"class": "range-revamp-product-compact__wrapper-link"}
    )
    if prod_url_tag:
        prod_url = prod_url_tag["href"]
    else:
        log.info("No se pudo obtener prod_url")

    # prod_list_info = [prod_name,desc_1,desc_2,price_1,price_2,status_1,status_2,prod_url]

    product_info = {
        "prod_category": prod_category,
        "prod_url": prod_url,
        # "prod_name": prod_name,
        "desc_1": desc_1,
        "desc_2": desc_2,
        "price_1": float(price_1.replace(" ", "")),
        "price_2": price_2,
        # "status_1": status_1,
        # "status_2": status_2,
    }
    return product_info


def start_scraping(url, connection):
    """
    Scraping main function, orchestrates full process
    """

    # En este bloque se obtienen los productos de la categoria.
    # Para esto se hace obtienen primero el total de productos en
    # la parte de abajo de la primera página. Como hay 24 productos max por
    # pagina, dividimos el total por 24, obtenemos el total de paginas,
    # y abrimos la pagina con todos los productos.
    # Finalmente, hacemos otro smooth scroll de toda la pagina, para que carguen
    # todos los productos.
    log.info("")
    log.info(">>>>>>>> CATEGORIA: %s", url)
    log.info("")
    get_dom(url)
    do_smooth_scroll()
    cat_soup = BeautifulSoup(driver.page_source, "html.parser")
    prod_category = cat_soup.find(
        "h1", {"class": "plp-page-title__title"}).text.strip()
    total_products_tag = cat_soup.find(
        "div", {"class": "catalog-product-list__total-count"}
    )
    if total_products_tag:
        total_products = re.findall(
            r"\d+", total_products_tag.text.strip())[-1]
        pages = int(math.ceil(float(total_products) / 24))
        full_prods_url = url + "?page=" + str(pages)
        get_dom(full_prods_url)
        do_smooth_scroll()
        driver.execute_script("window.scrollTo(0, 1080);")
        cat_soup = BeautifulSoup(driver.page_source, "html.parser")

        results = cat_soup.find_all("div", {"data-testid": "plp-product-card"})

        log.info("total_products_tag: %s", total_products_tag)
        log.info("total products: %s", total_products)
        log.info("pages: %i", pages)
        log.info("full_prods_url: %s", full_prods_url)
        log.info("results: %s", str(len(results)))
    else:
        results = []
        log.info(">>> Página sin productos listados.")
        log.info(url)
        log.info(">>>")

    # Obtener los detalles de cada producto, desde la pagina de resultados
    products = []
    added_products = []
    i = 0
    for result in results:
        product_info = []
        log.info(">>>>>>>>>>>>>>>>>>>>")
        log.info("Producto: %i", i)

        has_variants_tag = result.find(
            "div", {"class": "plp-product-thumbnails"})
        if has_variants_tag:
            log.info("Tiene variantes!")
            prod_variants = has_variants_tag.find_all("a", {"": ""})
            num_variants = 0
            for variant in prod_variants:
                num_variants = num_variants + 1
                if num_variants < 6:
                    var_xpath = xpath_soup(variant)
                    log.info(">>")
                    log.info("Variant xpath!!!")
                    log.info(var_xpath)
                    variant_sele_element = driver.find_element_by_xpath(
                        var_xpath)
                    # log.info("Moving...")
                    # driver.execute_script("arguments[0].scrollIntoView();", variant_sele_element)
                    # time.sleep(2)
                    # actions.move_to_element(variant_sele_element).perform()
                    # log.info("Stopped moving...")
                    try:
                        variant_sele_element.click()
                        log.info("Variant button clicked ok")
                        time.sleep(0.5)

                        var_soup = BeautifulSoup(
                            driver.page_source, "html.parser")
                        var_results = var_soup.find_all(
                            "div", {"data-testid": "plp-product-card"}
                        )

                        product_info = []
                        product_info = get_prod_details_from_listing(
                            prod_category, var_results[i]
                        )
                        if product_info["prod_url"] not in added_products:
                            products.append(product_info)
                            log.info("Nuevo producto agregado: ")
                            log.info(product_info)
                            added_products.append(product_info["prod_url"])
                        else:
                            log.info("Producto duplicado: ")
                            log.info(product_info)
                    except Exception as ex:
                        log.info("Error al obtener variante")
                        log.error(ex)
        else:
            product_info = get_prod_details_from_listing(prod_category, result)
            if product_info["prod_url"] not in added_products:
                products.append(product_info)
                log.info("Nuevo producto agregado: ")
                log.info(product_info)
                added_products.append(product_info["prod_url"])
            else:
                log.info("Producto duplicado: ")
                log.info(product_info)

        i = i + 1

    # Obtener detalles desde la página del producto
    for product in products:
        get_dom(product["prod_url"])
        time.sleep(1)
        prod_soup = BeautifulSoup(driver.page_source, "html.parser")

        # NAME
        try:
            prod_name = (
                prod_soup.find(
                    "span",
                    {"class": "pip-header-section__title--big notranslate"},
                    # {"class": "range-revamp-header-section__title--small notranslate"},
                )
            ).text.strip()
            log.info(prod_name)
            product["prod_name"] = prod_name
        except Exception as ex:
            log.info("No se pudo obtener nombre")
            log.error(ex)

        # PICTURES
        pictures = []
        picture_tags = prod_soup.find_all(
            "img", {"class": "pip-aspect-ratio-image__image"}
            # "img", {"class": "range-revamp-aspect-ratio-image__image"}
        )
        if picture_tags:
            max_pictures = 0
            for pic_tag in picture_tags:
                pictures.append(pic_tag["src"])
                max_pictures = max_pictures + 1
                if max_pictures >= 3:
                    break

            pics_json = json.dumps(pictures)
            product["pictures"] = pics_json
        else:
            log.info("No se pudieron obtener imagenes")

        # SUMMARY
        summary = ""
        summary_tag = prod_soup.find(
            "p", {"class": "pip-product-summary__description"}
            # "p", {"class": "range-revamp-product-summary__description"}
        )
        if summary_tag:
            summary = summary_tag.text.strip().replace("'", "")
            product["summary"] = summary
        else:
            log.info("No se pudo obtener summary")

        # PRODUCT NUMBER
        prod_number = ""
        prod_number_tag = prod_soup.find(
            "span", {"class": "pip-product-identifier__value"}
            # "span", {"class": "range-revamp-product-identifier__value"}
        )
        if prod_number_tag:
            prod_number = prod_number_tag.text.strip()
            product["prod_number"] = prod_number
        else:
            log.info("No se pudo obtener prod_number")

        # Online availability
        # try:
        #     status_1 = (prod_soup.find(
        #         "span", {"class": "pip-delivery__text"}
        #         # "span", {"class": "range-revamp-delivery__text"}
        #     ).text.strip().split("."))[0]
        #     product["status_1"] = status_1
        # except Exception as ex:
        #     log.info("No se pudo obtener online stock")
        #     log.error(ex)
        try:
            status_1 = prod_soup.find(
                "strong", {"class": "pip-delivery__text-header"}
            )
            if status_1:
                delivery_available = status_1.find("span", {"class": "pip-status pip-status--green pip-status--small"})
                if delivery_available:
                    product["status_1"] = "Disponible para entrega"
                else:
                    delivery_unavailable = status_1.find("span", {"class": "pip-status pip-status--red pip-status--small"})
                    if delivery_unavailable:
                        product["status_1"] = "No disponible para entrega"
                    else:
                        delivery_limited = status_1.find("span", {"class": "pip-status pip-status--orange pip-status--small"})
                        if delivery_limited:
                            product["status_1"] = "Disponible para entrega, pocas unidades"
                        else:
                            product["status_1"] = "No se pudo obtener disponibilidad para entrega"
        except Exception as ex:
            log.info("No se pudo obtener online stock")
            log.error(ex)

        # Store availability
        # try:
        #     status_2 = (prod_soup.find(
        #         "span", {"class": "pip-stockcheck__text"}
        #         # "span", {"class": "range-revamp-stockcheck__text"}
        #     ).text.strip().split("."))[0]
        #     product["status_2"] = status_2
        # except Exception as ex:
        #     log.info("No se pudo obtener store stock")
        #     log.error(ex)
        try:
            status_2 = prod_soup.find(
                "strong", {"class": "pip-stockcheck__text-header"}
            )
            if status_2:
                delivery_available = status_2.find("span", {"class": "pip-status pip-status--green pip-status--small"})
                if delivery_available:
                    product["status_2"] = "Disponible en tienda"
                else:
                    delivery_unavailable = status_2.find("span", {"class": "pip-status pip-status--red pip-status--small"})
                    if delivery_unavailable:
                        product["status_2"] = "No disponible en tienda"
                    else:
                        delivery_limited = status_2.find("span", {"class": "pip-status pip-status--orange pip-status--small"})
                        if delivery_limited:
                            product["status_2"] = "Disponible en tienda, pocas unidades"
                        else:
                            product["status_2"] = "No se pudo obtener disponibilidad en tienda"
        except Exception as ex:
            log.info("No se pudo obtener store stock")
            log.error(ex)

        # PRODUCT DIMENSIONS
        prod_dimensions = get_product_dimensions()
        if prod_dimensions:
            product["prod_dimensions"] = prod_dimensions
        else:
            log.info("No se pudo obtener prod_dimensions")

        product["scrape_datetime"] = datetime.now()

        insert_product(connection, product)
        log.info(product)
        log.info("")
        log.info("")
        log.info(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>")
        log.info("")
        log.info("")

    # log.info("Inserting to csv")
    # with open("ikea.csv", "a", encoding="utf-8", newline="") as csvoutput:
    #     csvwriter = csv.writer(csvoutput, delimiter=",")
    #     csvwriter.writerow(product)
    # log.info("")


def main():
    """
    Función main, ejecuta el proceso paso por paso
    """
    print("PROCESS START!")
    snowflake_configuration = {
        "url": "hg51401.snowflakecomputing.com",
        "account": "hg51401",
        "user": "ALAIN.MORE@RAPPI.COM",
        "authenticator": "externalbrowser",
        "port": 443,
        "warehouse": "GROWTH_ANALYSTS",
        "role": "GROWTH_ROLE",
        "database": "fivetran",
    }
    engine = create_engine(URL(**snowflake_configuration))
    connection = engine.connect()
    try:
        set_query_tag(connection)
        continue_process = cleanup_snowflake_tables(connection)
        if continue_process:
            with open("categories.csv", "r") as csvfile:
                csvreader = csv.DictReader(csvfile)
                for row in csvreader:
                    cat_url = row["category_url"]
                    start_scraping(cat_url, connection)

            driver.close()
            print("PROCESS FINISHED OK!")
        else:
            print("Error al respaldar y/o borrar data anterior")
    finally:
        connection.close()
        engine.dispose()
        log.info("Engine y connection cerrados")


if __name__ == "__main__":
    main()
